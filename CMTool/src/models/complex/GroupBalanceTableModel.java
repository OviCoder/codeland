package models.complex;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class GroupBalanceTableModel extends AbstractTableModel {

	private ArrayList<GroupBalance> rowData = new ArrayList<GroupBalance>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public GroupBalanceTableModel(final ArrayList<GroupBalance> gbs) {
		rowData = gbs;
		columnNames.add("Name");
		columnNames.add("Income");
		columnNames.add("Outcome");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return false;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {		
			GroupBalance rb = rowData.get(rowIndex);
			switch (columnIndex) {		
			case 0:								
				rb.setName((String)aValue);				
				break;
			case 1:				
				rb.setIncome(Integer.parseInt((String)aValue));
				break;		
			case 2:
				rb.setOutcome(Integer.parseInt((String)aValue));
				break;		
			}
		} catch (NumberFormatException e){
			
			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getName();
		case 1:
			return rowData.get(rowIndex).getIncome();	
		case 2:
			return rowData.get(rowIndex).getOutcome();	
		default:
			return null;
		}
	}
		
	public ArrayList<GroupBalance> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<GroupBalance> rowData) {
		this.rowData = rowData;
	}
}
