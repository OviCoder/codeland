package models.complex;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TableBalanceTableModel extends AbstractTableModel {

	private ArrayList<TableBalance> rowData = new ArrayList<TableBalance>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public TableBalanceTableModel(final ArrayList<TableBalance> tbs) {
		rowData = tbs;
		columnNames.add("Number");
		columnNames.add("Room Number");
		columnNames.add("Income");
		columnNames.add("Outcome");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return false;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {		
			TableBalance tb = rowData.get(rowIndex);
			switch (columnIndex) {		
			case 0:								
				tb.setNumber(Integer.parseInt((String)aValue));				
				break;
			case 1:								
				tb.setRoomNumber(Integer.parseInt((String)aValue));				
				break;
			case 2:				
				tb.setIncome(Integer.parseInt((String)aValue));
				break;		
			case 3:
				tb.setOutcome(Integer.parseInt((String)aValue));
				break;		
			}
		} catch (NumberFormatException e){
			
			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getNumber();
		case 1:
			return rowData.get(rowIndex).getRoomNumber();
		case 2:
			return rowData.get(rowIndex).getIncome();	
		case 3:
			return rowData.get(rowIndex).getOutcome();	
		default:
			return null;
		}
	}
		
	public ArrayList<TableBalance> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<TableBalance> rowData) {
		this.rowData = rowData;
	}
}
