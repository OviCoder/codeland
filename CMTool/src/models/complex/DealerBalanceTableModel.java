package models.complex;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class DealerBalanceTableModel extends AbstractTableModel {

	private ArrayList<DealerBalance> rowData = new ArrayList<DealerBalance>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public DealerBalanceTableModel(final ArrayList<DealerBalance> dbs) {
		rowData = dbs;
		columnNames.add("Name");
		columnNames.add("Income");
		columnNames.add("Outcome");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return false;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {		
			DealerBalance db = rowData.get(rowIndex);
			switch (columnIndex) {		
			case 0:								
				db.setName((String)aValue);				
				break;
			case 1:				
				db.setIncome(Integer.parseInt((String)aValue));
				break;		
			case 2:
				db.setOutcome(Integer.parseInt((String)aValue));
				break;		
			}
		} catch (NumberFormatException e){
			
			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getName();
		case 1:
			return rowData.get(rowIndex).getIncome();	
		case 2:
			return rowData.get(rowIndex).getOutcome();	
		default:
			return null;
		}
	}
		
	public ArrayList<DealerBalance> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<DealerBalance> rowData) {
		this.rowData = rowData;
	}
}
