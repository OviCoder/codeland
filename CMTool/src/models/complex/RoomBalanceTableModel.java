package models.complex;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class RoomBalanceTableModel extends AbstractTableModel {

	private ArrayList<RoomBalance> rowData = new ArrayList<RoomBalance>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public RoomBalanceTableModel(final ArrayList<RoomBalance> rbs) {
		rowData = rbs;
		columnNames.add("Number");
		columnNames.add("Income");
		columnNames.add("Outcome");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return false;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {		
			RoomBalance rb = rowData.get(rowIndex);
			switch (columnIndex) {		
			case 0:								
				rb.setNumber(Integer.parseInt((String)aValue));				
				break;
			case 1:				
				rb.setIncome(Integer.parseInt((String)aValue));
				break;		
			case 2:
				rb.setOutcome(Integer.parseInt((String)aValue));
				break;		
			}
		} catch (NumberFormatException e){
			
			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getNumber();
		case 1:
			return rowData.get(rowIndex).getIncome();	
		case 2:
			return rowData.get(rowIndex).getOutcome();	
		default:
			return null;
		}
	}
		
	public ArrayList<RoomBalance> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<RoomBalance> rowData) {
		this.rowData = rowData;
	}
}
