package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class RoomTableModel extends AbstractTableModel {

	private ArrayList<Room> rowData = new ArrayList<Room>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public RoomTableModel(final ArrayList<Room> rooms) {
		rowData = rooms;
		columnNames.add("Number");
		columnNames.add("Tables capacity");
		columnNames.add("Last modified by");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return (columnIndex != 0);
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {
			
			Room r = rowData.get(rowIndex);
			switch (columnIndex) {
			case 1:
				if(r.getTableCapacity() != Integer.parseInt((String)aValue)){
					r.hasChanges = true;
				}
				r.setTableCapacity(Integer.parseInt((String)aValue));
				break;		
			}
		} catch (NumberFormatException e){
			
			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getNumber();
		case 1:
			return rowData.get(rowIndex).getTableCapacity();	
		case 2:
			return rowData.get(rowIndex).getLastModifiedBy();
		default:
			return null;
		}
	}

	public Room getRoomAt(final int rowIndex){
		
		return rowData.get(rowIndex);
	}
	
	public ArrayList<Room> getModifiedRooms(){
		ArrayList<Room> modifiedRooms = new ArrayList<Room>();
		
		for(Room r : rowData){
			if(r.hasChanges){
				modifiedRooms.add(r);
			}
		}
		return modifiedRooms;
	}
	
	public ArrayList<Room> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<Room> rowData) {
		this.rowData = rowData;
	}
}
