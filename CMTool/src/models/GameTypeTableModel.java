package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class GameTypeTableModel extends AbstractTableModel {
	private ArrayList<GameType> rowData = new ArrayList<GameType>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public GameTypeTableModel(final ArrayList<GameType> gameTypes) {
		rowData = gameTypes;
		columnNames.add("Name");
		columnNames.add("Details");
		columnNames.add("Last modified by");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 0)
			return false;
		return true;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {

			GameType gt = rowData.get(rowIndex);

			if (columnIndex == 1) {
				if (gt.getDetails() != (String) aValue) {
					gt.hasChanges = true;
				}

				gt.setDetails((String) aValue);
			}
		} catch (NumberFormatException e) {

			System.out.println(e.getMessage());
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getName();
		case 1:
			return rowData.get(rowIndex).getDetails();
		case 2:
			return rowData.get(rowIndex).getLastModifiedBy();
		default:
			return null;
		}
	}

	public ArrayList<GameType> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<GameType> rowData) {
		this.rowData = rowData;
	}

	public GameType getGameTypeAt(final int rowIndex) {

		return rowData.get(rowIndex);
	}

	public ArrayList<GameType> getModifiedGameTypes() {
		ArrayList<GameType> modifiedGameTypes = new ArrayList<GameType>();

		for (GameType gt : rowData) {
			if (gt.hasChanges) {
				modifiedGameTypes.add(gt);
			}
		}
		return modifiedGameTypes;
	}
}
