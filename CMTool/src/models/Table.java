package models;

public class Table {

	private int id;
	private int number;
	private int roomNumber;
	private int sitsCapacity;
	private String lastModifiedBy;
	public boolean hasChanges = false;
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getRoomNumber() {
		return roomNumber;
	}
	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	public int getSitsCapacity() {
		return sitsCapacity;
	}
	public void setSitsCapacity(int sitsCapacity) {
		this.sitsCapacity = sitsCapacity;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
}
