package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class DealerTableModel extends AbstractTableModel {

	private ArrayList<Dealer> rowData = new ArrayList<Dealer>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public DealerTableModel(final ArrayList<Dealer> dealers) {
		rowData = dealers;
		columnNames.add("Name");
		columnNames.add("Grade");
		columnNames.add("Last modified by");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return true;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {
			
			Dealer d = rowData.get(rowIndex);
			switch (columnIndex) {
			
			case 0:				
				if(d.getName() != (String)aValue){
					d.hasChanges = true;
				}
				d.setName((String) aValue);				
				break;
			case 1:
				if(d.getGrade() != (String)aValue){
					d.hasChanges = true;
				}
				d.setGrade((String) aValue);
				break;		
			}
		} catch (NumberFormatException e){
			
			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getName();
		case 1:
			return rowData.get(rowIndex).getGrade();	
		case 2:
			return rowData.get(rowIndex).getLastModifiedBy();	
		default:
			return null;
		}
	}

	public Dealer getDealerAt(final int rowIndex){
		
		return rowData.get(rowIndex);
	}
	
	public ArrayList<Dealer> getModifiedDealers(){
		ArrayList<Dealer> modifiedDealers = new ArrayList<Dealer>();
		
		for(Dealer d : rowData){
			if(d.hasChanges){
				modifiedDealers.add(d);
			}
		}
		return modifiedDealers;
	}
	
	public ArrayList<Dealer> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<Dealer> rowData) {
		this.rowData = rowData;
	}
}
