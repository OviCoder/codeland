package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TableTableModel extends AbstractTableModel {

	private ArrayList<Table> rowData = new ArrayList<Table>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public TableTableModel(final ArrayList<Table> tables) {
		rowData = tables;
		columnNames.add("Number");
		columnNames.add("Room number");
		columnNames.add("Sits capacity");
		columnNames.add("Last modified by");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		return true;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {		
			Table t = rowData.get(rowIndex);
			switch (columnIndex) {
			
			case 0:				
				if(t.getNumber() != Integer.parseInt((String)aValue)){
					t.hasChanges = true;
				}
				t.setNumber(Integer.parseInt((String)aValue));				
				break;
			case 1:
				if(t.getRoomNumber() != Integer.parseInt((String)aValue)){
					t.hasChanges = true;
				}
				t.setRoomNumber(Integer.parseInt((String)aValue));
				break;		
			case 2:
				if(t.getSitsCapacity() != Integer.parseInt((String)aValue)){
					t.hasChanges = true;
				}
				t.setSitsCapacity(Integer.parseInt((String)aValue));
				break;		
			}
		} catch (NumberFormatException e){
			
			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getNumber();
		case 1:
			return rowData.get(rowIndex).getRoomNumber();	
		case 2:
			return rowData.get(rowIndex).getSitsCapacity();	
		case 3:
			return rowData.get(rowIndex).getLastModifiedBy();
		default:
			return null;
		}
	}

	public Table getTableAt(final int rowIndex){
		
		return rowData.get(rowIndex);
	}
	
	public ArrayList<Table> getModifiedTables(){
		ArrayList<Table> modifiedTables = new ArrayList<Table>();
		
		for(Table t : rowData){
			if(t.hasChanges){
				modifiedTables.add(t);
			}
		}
		return modifiedTables;
	}
	
	public ArrayList<Table> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<Table> rowData) {
		this.rowData = rowData;
	}
}
