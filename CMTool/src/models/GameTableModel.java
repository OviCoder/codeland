package models;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class GameTableModel extends AbstractTableModel {

	private ArrayList<Game> rowData = new ArrayList<Game>();
	private ArrayList<String> columnNames = new ArrayList<String>();

	public GameTableModel(final ArrayList<Game> games) {

		rowData = games;
		columnNames.add("Name");
		columnNames.add("Room No.");
		columnNames.add("Table No.");
		columnNames.add("Start");
		columnNames.add("End");
		columnNames.add("Finished");
		columnNames.add("Income");
		columnNames.add("Outcome");
		columnNames.add("Dealer");
		columnNames.add("Last modified by");
	}

	public int getColumnCount() {
		return columnNames.size();
	}

	public int getRowCount() {
		return rowData.size();
	}

	public String getColumnName(int col) {
		return columnNames.get(col);
	}

	// all cells are editable
	public boolean isCellEditable(int rowIndex, int columnIndex) {

		if (columnIndex == 0 || columnIndex == 3 || columnIndex == 4 || columnIndex == 8 || (columnIndex == 5 && (rowData.get(rowIndex).getFinished() == "Y")))
			return false;

		return true;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

		try {
			Game g = rowData.get(rowIndex);
			switch (columnIndex) {
			case 1:
				if(g.getRoom() != Integer.parseInt((String)aValue)){
					g.hasChanges = true;
				}
				rowData.get(rowIndex).setRoom(Integer.parseInt((String)aValue));
				break;
			case 2:
				if(g.getTable() != Integer.parseInt((String)aValue)){
					g.hasChanges = true;
				}
				rowData.get(rowIndex).setTable(Integer.parseInt((String)aValue));
				break;
			case 5:
				if(g.getFinished() != (String)aValue){
					g.hasChanges = true;
				}
				rowData.get(rowIndex).setFinished((String) aValue);
				break;
			case 6:
				if(g.getIncome() != Integer.parseInt((String)aValue)){
					g.hasChanges = true;
				}
				rowData.get(rowIndex).setIncome(Integer.parseInt((String)aValue));
				break;
			case 7:
				if(g.getOutcome() != Integer.parseInt((String)aValue)){
					g.hasChanges = true;
				}
				rowData.get(rowIndex).setOutcome(Integer.parseInt((String)aValue));
				break;
			}
		} catch (NumberFormatException e) {

			System.out.println(e.getMessage());
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {

		switch (columnIndex) {
		case 0:
			return rowData.get(rowIndex).getGameTypeName();
		case 1:
			return rowData.get(rowIndex).getRoom();
		case 2:
			return rowData.get(rowIndex).getTable();
		case 3:
			return rowData.get(rowIndex).getStartDate();
		case 4:
			return rowData.get(rowIndex).getEndDate();
		case 5:
			return rowData.get(rowIndex).getFinished();
		case 6:
			return rowData.get(rowIndex).getIncome();
		case 7:
			return rowData.get(rowIndex).getOutcome();
		case 8:
			return rowData.get(rowIndex).getDealerName();
		case 9:
			return rowData.get(rowIndex).getLastModifiedBy();
		default:
			return null;
		}
	}

	public ArrayList<Game> getRowData() {
		return rowData;
	}

	public void setRowData(ArrayList<Game> rowData) {
		this.rowData = rowData;
	}

	public Game getGameAt(final int rowIndex) {

		return rowData.get(rowIndex);
	}

	public ArrayList<Game> getModifiedGames() {
		ArrayList<Game> modifiedGames = new ArrayList<Game>();

		for (Game g : rowData) {
			if (g.hasChanges) {
				modifiedGames.add(g);
			}
		}
		return modifiedGames;
	}

}
