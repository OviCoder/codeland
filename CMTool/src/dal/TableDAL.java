package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import application.UserContext;
import models.Table;

public class TableDAL extends AbstractDAL {
	public TableDAL() {
		super();
	}

	public void addTable(final Table table) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection
					.prepareStatement("INSERT INTO [dbo].[TABLE](NUMBER, ROOM_NUMBER, SITS_CAPACITY, LAST_MODIFIED_BY) VALUES(?, ?, ?, ?)");
			statement.setInt(1, table.getNumber());
			statement.setInt(2, table.getRoomNumber());
			statement.setInt(3, table.getSitsCapacity());
			statement.setString(4, UserContext.currentUser);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when adding table");
		}
	}

	public void deleteTable(final int selectedEntityID) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("DELETE FROM [dbo].[TABLE] WHERE [ID] = ?");
			statement.setInt(1, selectedEntityID);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when deleting table");
		}
	}

	public boolean updateTables(final ArrayList<Table> tables) {
		Connection sqlConnection = getDbConnection();
		try {
			if (tables.size() > 0) {
				for (Table t : tables) {
					PreparedStatement statement = sqlConnection
							.prepareStatement("UPDATE [dbo].[TABLE] SET [NUMBER] = ?, [ROOM_NUMBER] = ?, [SITS_CAPACITY] = ?, [LAST_MODIFIED_BY] = ? WHERE [ID] = ?");
					statement.setInt(1, t.getNumber());
					statement.setInt(2, t.getRoomNumber());
					statement.setInt(3, t.getSitsCapacity());
					statement.setString(4, UserContext.currentUser);
					statement.setInt(5, t.getId());
					statement.executeUpdate();
					t.hasChanges = false;
				}
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error when updating tables");
		}

		return false;
	}
	
	public ArrayList<Table> getAllTables(){
		ArrayList<Table> tables = new ArrayList<Table>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT * FROM [dbo].[TABLE]");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				Table t = new Table();
				t.setId(result.getInt("ID"));
				t.setNumber(result.getInt("NUMBER"));
				t.setRoomNumber(result.getInt("ROOM_NUMBER"));
				t.setSitsCapacity(result.getInt("SITS_CAPACITY"));
				t.setLastModifiedBy(result.getString("LAST_MODIFIED_BY"));
				tables.add(t);
			}
			return tables;
		} catch (SQLException e) {
			System.out.println("Error when getting all tables");
		}
		
		return null;
	}
}
