package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import models.complex.DealerBalance;
import models.complex.GroupBalance;
import models.complex.RoomBalance;
import models.complex.TableBalance;

public class ComplexDAL extends AbstractDAL{
	public ComplexDAL(){
		super();
	}
	
	public ArrayList<DealerBalance> getDealersBalance(){
		ArrayList<DealerBalance> dbs = new ArrayList<DealerBalance>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT NAME, SUM(INCOME) AS INCOME, SUM(OUTCOME) AS OUTCOME "
																		+ "FROM DEALER LEFT JOIN GAME ON DEALER.NAME = GAME.DEALER_NAME "
																		+ "GROUP BY NAME");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				DealerBalance db = new DealerBalance();				
				db.setName(result.getString("NAME"));
				db.setIncome(result.getInt("INCOME"));
				db.setOutcome(result.getInt("OUTCOME"));
				
				dbs.add(db);
			}
			return dbs;
		} catch (SQLException e) {
			System.out.println("Error when getting all dealer balances");
		}	
		return null;
	}
	
	public ArrayList<RoomBalance> getRoomsBalance(){
		ArrayList<RoomBalance> rbs = new ArrayList<RoomBalance>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT NUMBER, SUM(INCOME) AS INCOME, SUM(OUTCOME) AS OUTCOME "
																		+ "FROM ROOM LEFT JOIN GAME ON NUMBER = GAME.ROOM_NUMBER "
																		+ "GROUP BY NUMBER");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				RoomBalance rb = new RoomBalance();
				rb.setNumber(result.getInt("NUMBER"));
				rb.setIncome(result.getInt("INCOME"));
				rb.setOutcome(result.getInt("OUTCOME"));
				
				rbs.add(rb);
			}
			return rbs;
		} catch (SQLException e) {
			System.out.println("Error when getting all room balances");
		}	
		return null;
	}
	
	public ArrayList<GroupBalance> getGroupsBalance(){
		ArrayList<GroupBalance> gbs = new ArrayList<GroupBalance>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT NAME, SUM(INCOME) AS INCOME, SUM(OUTCOME) AS OUTCOME "
																		+ "FROM GAME_TYPE LEFT JOIN GAME ON NAME = GAME.GAME_TYPE "
																		+ "GROUP BY NAME");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				GroupBalance gb = new GroupBalance();
				gb.setName(result.getString("NAME"));
				gb.setIncome(result.getInt("INCOME"));
				gb.setOutcome(result.getInt("OUTCOME"));
				
				gbs.add(gb);
			}
			return gbs;
		} catch (SQLException e) {
			System.out.println("Error when getting all groups balances");
		}	
		return null;
	}
	
	public ArrayList<TableBalance> getTablesBalance(){
		ArrayList<TableBalance> tbs = new ArrayList<TableBalance>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT [TABLE].NUMBER, [TABLE].ROOM_NUMBER, SUM(INCOME) AS INCOME, SUM(OUTCOME) AS OUTCOME " 
																		+"FROM [TABLE] "
																		+"LEFT JOIN GAME ON "
																		+"[TABLE].NUMBER = GAME.TABLE_NUMBER AND [TABLE].ROOM_NUMBER = GAME.ROOM_NUMBER "
																		+"GROUP BY [TABLE].NUMBER, [TABLE].ROOM_NUMBER");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				TableBalance tb = new TableBalance();
				tb.setNumber(result.getInt("NUMBER"));
				tb.setRoomNumber(result.getInt("ROOM_NUMBER"));
				tb.setIncome(result.getInt("INCOME"));
				tb.setOutcome(result.getInt("OUTCOME"));
				
				tbs.add(tb);
			}
			return tbs;
		} catch (SQLException e) {
			System.out.println("Error when getting all groups balances");
		}	
		return null;
	}
}
