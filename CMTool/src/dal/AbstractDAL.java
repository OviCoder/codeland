package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class AbstractDAL {

	protected static final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	protected static final String DB_URL = "jdbc:sqlserver://OVI\\OVI;DatabaseName=CMTool;integratedSecurity=true";
	
	public AbstractDAL(){	
		try {
			Class.forName(JDBC_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println("Error when getting the class of the JDBC_DRIVER");
		}
	}
	
	protected Connection getDbConnection(){		
		Connection dbConnection = null;
		
		try {		
			dbConnection = DriverManager.getConnection(DB_URL);
		} catch (SQLException e) {
			System.out.println("Error when getting connection");
		}
			
		return dbConnection;
	}
}
