package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.Account;

public class AuthenticationDAL extends AbstractDAL {
	public AuthenticationDAL() {
		super();
	}

	public Account requestAccount(String user) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection
					.prepareStatement("SELECT * FROM [dbo].[ACCOUNT] WHERE [NAME] = ?");
			statement.setString(1, user);
			ResultSet result = statement.executeQuery();
			if (result.next()) {
				Account a = new Account();
				a.setUsername(result.getString("NAME"));
				a.setPassword(result.getString("PASSWORD"));
				a.setFailures(result.getInt("FAILURES"));
				return a;
			}
		} catch (SQLException e) {
			System.out.println("Error when logging in");
		}
		return null;
	}

	public void updateAccount(Account a) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection
					.prepareStatement("UPDATE [dbo].[ACCOUNT] SET [FAILURES] = ? WHERE [NAME] = ?");
			statement.setInt(1, a.getFailures());
			statement.setString(2, a.getUsername());
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when logging in");
		}
	}
}
