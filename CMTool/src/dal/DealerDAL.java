package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import application.UserContext;
import models.Dealer;

public class DealerDAL extends AbstractDAL{
	public DealerDAL(){		
		super();
	}
	
	public void addDealer(final Dealer dealer){
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("INSERT INTO [dbo].[DEALER](NAME, RANK, LAST_MODIFIED_BY) VALUES(?, ?, ?)");
			statement.setString(1, dealer.getName());
			statement.setString(2, dealer.getGrade());
			statement.setString(3, UserContext.currentUser);
			statement.executeUpdate();		
		} catch (SQLException e) {
			System.out.println("Error when adding dealer");
		}			
	}
	
	public void deleteDealer(final int selectedEntityID){
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("DELETE FROM [dbo].[DEALER] WHERE [ID] = ?");
			statement.setInt(1, selectedEntityID);
			statement.executeUpdate();		
		} catch (SQLException e) {
			System.out.println("Error when deleting dealer");
		}			
	}
	
	public boolean updateDealers(final ArrayList<Dealer> dealers){
		
		Connection sqlConnection = getDbConnection();
		try {			
			if(dealers.size() > 0){
				for(Dealer d : dealers){
					PreparedStatement statement = sqlConnection.prepareStatement("UPDATE [dbo].[DEALER] SET [NAME] = ?, [RANK] = ?, [LAST_MODIFIED_BY] = ? WHERE [ID] = ?");
					statement.setString(1, d.getName());
					statement.setString(2, d.getGrade());
					statement.setString(3, UserContext.currentUser);
					statement.setInt(4, d.getId());
					statement.executeUpdate();	
					d.hasChanges = false;
				}
				return true;
			}			
		} catch (SQLException e) {
			System.out.println("Error when updating dealer");
		}
		
		return false;
	}
	
	public ArrayList<Dealer> getAllDealers(){
		ArrayList<Dealer> dealers = new ArrayList<Dealer>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT * FROM [dbo].[DEALER]");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				Dealer d = new Dealer();
				d.setId(result.getInt("ID"));
				d.setName(result.getString("NAME"));
				d.setGrade(result.getString("RANK"));
				d.setLastModifiedBy(result.getString("LAST_MODIFIED_BY"));
				dealers.add(d);
			}
			return dealers;
		} catch (SQLException e) {
			System.out.println("Error when getting all dealers");
		}
		
		return null;
	}
}
