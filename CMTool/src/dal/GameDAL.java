package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import application.UserContext;
import models.Game;

public class GameDAL extends AbstractDAL {
	public GameDAL() {
		super();
	}

	public void addGame(final Game game) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement(
					"INSERT INTO [dbo].[GAME](TABLE_NUMBER, GAME_TYPE, START_DATE, FINISHED, ROOM_NUMBER, DEALER_NAME, LAST_MODIFIED_BY) VALUES(?, ?, GETDATE(), 'N', ?, ?, ?)");
			statement.setInt(1, game.getTable());
			statement.setString(2, game.getGameTypeName());
			statement.setInt(3, game.getRoom());
			statement.setString(4, game.getDealerName());
			statement.setString(5, UserContext.currentUser);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when adding game");
		}
	}

	public boolean updateGames(final ArrayList<Game> games) {
		Connection sqlConnection = getDbConnection();
		try {
			if (games.size() > 0) {
				for (Game g : games) {
					PreparedStatement statement;
					if (g.getFinished().equals("Y")) {
						statement = sqlConnection.prepareStatement(
								"UPDATE [dbo].[GAME] SET [TABLE_NUMBER] = ?, [END_DATE] = GETDATE(), [FINISHED] = ?, [ROOM_NUMBER] = ?, [INCOME] = ?, [OUTCOME] = ?, [LAST_MODIFIED_BY] = ? WHERE [ID] = ?");
					} else {
						statement = sqlConnection.prepareStatement(
								"UPDATE [dbo].[GAME] SET [TABLE_NUMBER] = ?, [FINISHED] = ?, [ROOM_NUMBER] = ?, [INCOME] = ?, [OUTCOME] = ?, [LAST_MODIFIED_BY] = ? WHERE [ID] = ?");
					}
					statement.setInt(1, g.getTable());
					statement.setString(2, g.getFinished());
					statement.setInt(3, g.getRoom());
					statement.setInt(4, g.getIncome());
					statement.setInt(5, g.getOutcome());
					statement.setString(6, UserContext.currentUser);
					statement.setInt(7, g.getId());
					statement.executeUpdate();
					g.hasChanges = false;
				}
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Error when updating game");
		}

		return false;
	}

	public ArrayList<Game> getAllGames() {
		ArrayList<Game> games = new ArrayList<Game>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT * FROM [dbo].[GAME]");
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				Game g = new Game();
				g.setId(result.getInt("ID"));
				g.setTable(result.getInt("TABLE_NUMBER"));
				g.setGameTypeName(result.getString("GAME_TYPE"));
				g.setStartDate(result.getString("START_DATE"));
				g.setEndDate(result.getString("END_DATE"));
				g.setFinished(result.getString("FINISHED"));
				g.setRoom(result.getInt("ROOM_NUMBER"));
				g.setDealerName(result.getString("DEALER_NAME"));
				g.setIncome(result.getInt("INCOME"));
				g.setOutcome(result.getInt("OUTCOME"));
				g.setLastModifiedBy(result.getString("LAST_MODIFIED_BY"));
				games.add(g);
			}
			return games;
		} catch (SQLException e) {
			System.out.println("Error when getting all games");
		}

		return null;
	}
}
