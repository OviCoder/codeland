package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import application.UserContext;
import models.GameType;

public class GameTypeDAL extends AbstractDAL {
	public GameTypeDAL() {
		super();
	}

	public void addGameType(final GameType gameType) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection
					.prepareStatement("INSERT INTO [dbo].[GAME_TYPE](NAME, DETAILS, LAST_MODIFIED_BY) VALUES(?, ?, ?)");
			statement.setString(1, gameType.getName());
			statement.setString(2, gameType.getDetails());
			statement.setString(3, UserContext.currentUser);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when adding game type");
		}
	}

	public void deleteGameType(final String selectedEntityName) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection
					.prepareStatement("DELETE FROM [dbo].[GAME_TYPE] WHERE [NAME] = ?");
			statement.setString(1, selectedEntityName);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when deleting game type");
		}
	}

	public boolean updateGameTypes(final ArrayList<GameType> gameTypes) {

		Connection sqlConnection = getDbConnection();
		try {
			if (gameTypes.size() > 0) {
				for (GameType gt : gameTypes) {
					PreparedStatement statement = sqlConnection
							.prepareStatement("UPDATE [dbo].[GAME_TYPE] SET [DETAILS] = ?, [LAST_MODIFIED_BY] = ? WHERE [NAME] = ?");
					statement.setString(1, gt.getDetails());
					statement.setString(2, UserContext.currentUser);
					statement.setString(3, gt.getName());
					statement.executeUpdate();
					gt.hasChanges = false;
				}
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Error when updating game types");
		}

		return false;
	}
	
	public ArrayList<GameType> getAllGameTypes(){
		ArrayList<GameType> gameTypes = new ArrayList<GameType>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT * FROM [dbo].[GAME_TYPE]");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				GameType gt = new GameType();
				gt.setName(result.getString("NAME"));
				gt.setDetails(result.getString("DETAILS"));
				gt.setLastModifiedBy(result.getString("LAST_MODIFIED_BY"));
				gameTypes.add(gt);
			}
			return gameTypes;
		} catch (SQLException e) {
			System.out.println("Error when getting all game types");
		}
		
		return null;
	}
}
