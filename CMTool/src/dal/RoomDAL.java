package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import application.UserContext;
import models.Room;

public class RoomDAL extends AbstractDAL {
	public RoomDAL() {
		super();
	}

	public void addRoom(final Room room) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection
					.prepareStatement("INSERT INTO [dbo].[ROOM](NUMBER, TABLE_CAPACITY, LAST_MODIFIED_BY) VALUES(?, ?, ?)");
			statement.setInt(1, room.getNumber());
			statement.setInt(2, room.getTableCapacity());
			statement.setString(3, UserContext.currentUser);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when adding room");
		}
	}

	public void deleteRoom(final int selectedEntityNumber) {
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("DELETE FROM [dbo].[ROOM] WHERE [NUMBER] = ?");
			statement.setInt(1, selectedEntityNumber);
			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error when deleting room");
		}
	}

	public boolean updateRooms(final ArrayList<Room> rooms) {

		Connection sqlConnection = getDbConnection();
		try {
			if (rooms.size() > 0) {
				for (Room r : rooms) {
					PreparedStatement statement = sqlConnection
							.prepareStatement("UPDATE [dbo].[ROOM] SET [TABLE_CAPACITY] = ?, [LAST_MODIFIED_BY] = ? WHERE [NUMBER] = ?");					
					statement.setInt(1, r.getTableCapacity());
					statement.setString(2, UserContext.currentUser);
					statement.setInt(3, r.getNumber());
					statement.executeUpdate();
					r.hasChanges = false;
				}
				return true;
			}
		} catch (SQLException e) {
			System.out.println("Error when updating rooms");
		}
		return false;
	}
	
	public ArrayList<Room> getAllRooms(){
		ArrayList<Room> rooms = new ArrayList<Room>();
		Connection sqlConnection = getDbConnection();
		try {
			PreparedStatement statement = sqlConnection.prepareStatement("SELECT * FROM [dbo].[ROOM]");
			ResultSet result = statement.executeQuery();
			while(result.next()){
				Room r = new Room();
				r.setNumber(result.getInt("NUMBER"));
				r.setTableCapacity(result.getInt("TABLE_CAPACITY"));
				r.setLastModifiedBy(result.getString("LAST_MODIFIED_BY"));
				rooms.add(r);
			}
			return rooms;
		} catch (SQLException e) {
			System.out.println("Error when getting all rooms");
		}
		
		return null;
	}
}
