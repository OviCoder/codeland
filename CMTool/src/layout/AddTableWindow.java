package layout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controllers.TableController;
import dal.RoomDAL;
import models.Room;
import models.Table;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JComboBox;

@SuppressWarnings("serial")
public class AddTableWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	
	public AddTableWindow() {
		setBounds(100, 100, 180, 188);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNumber = new JLabel("Number");
		
		JLabel lblRoomNumber = new JLabel("Room number");
		
		JLabel lblSitsCapacity = new JLabel("Sits capacity");
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JSpinner spinner = new JSpinner();
		
		JComboBox<Integer> comboBox = new JComboBox<Integer>();
		RoomDAL rc = new RoomDAL();
		ArrayList<Room> rooms = rc.getAllRooms();
		for(Room r : rooms){
			comboBox.addItem(r.getNumber());
		}
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TableController tc = new TableController();
				Table t = new Table();
				t.setNumber(Integer.parseInt(textField.getText()));
				t.setRoomNumber((int)comboBox.getSelectedItem());
				t.setSitsCapacity((int)spinner.getValue());
				tc.addTable(t);			
				dispose();
			}
		});
		
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblSitsCapacity)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(spinner, GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE))
								.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNumber)
										.addComponent(lblRoomNumber))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(textField, 0, 0, Short.MAX_VALUE)
										.addComponent(comboBox, 0, 44, Short.MAX_VALUE)))))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(48)
							.addComponent(btnAdd, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(20)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNumber)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblRoomNumber))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblSitsCapacity))
					.addPreferredGap(ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
					.addComponent(btnAdd)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
