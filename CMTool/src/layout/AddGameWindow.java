package layout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controllers.GameController;
import dal.DealerDAL;
import dal.GameTypeDAL;
import dal.RoomDAL;
import dal.TableDAL;
import models.Dealer;
import models.Game;
import models.GameType;
import models.Room;
import models.Table;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class AddGameWindow extends JFrame {

	private JPanel contentPane;

	public AddGameWindow() {
		setBounds(100, 100, 236, 195);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
			
		JComboBox<String> comboBox = new JComboBox<String>();
		GameTypeDAL gtd = new GameTypeDAL();
		ArrayList<GameType> gameTypes = gtd.getAllGameTypes();
		for(GameType gt : gameTypes){
			comboBox.addItem(gt.getName());
		}
		comboBox.setSelectedIndex(-1);
		
		JLabel lblName = new JLabel("Name");
		
		JComboBox<Integer> comboBox_1 = new JComboBox<Integer>();
		RoomDAL rd = new RoomDAL();
		ArrayList<Room> rooms = rd.getAllRooms();
		for(Room r : rooms){
			comboBox_1.addItem(r.getNumber());
		}
		comboBox_1.setSelectedIndex(-1);
			
		JLabel lblRoom = new JLabel("Room");
		
		JLabel lblTable = new JLabel("Table");
		
		JComboBox<Integer> comboBox_2 = new JComboBox<Integer>();
		
		JLabel lblDealer = new JLabel("Dealer");
		
		JComboBox<String> comboBox_3 = new JComboBox<String>();
		
		DealerDAL dd = new DealerDAL();
		ArrayList<Dealer> dealers = dd.getAllDealers();
		for(Dealer d : dealers){
			comboBox_3.addItem(d.getName());
		}
		comboBox_3.setSelectedIndex(-1);
		
		TableDAL td = new TableDAL();
		ArrayList<Table> tables = td.getAllTables();
		comboBox_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == comboBox_1) {
					comboBox_2.removeAllItems();
					for(Table t : tables){
						if(t.getRoomNumber() == (int)comboBox_1.getSelectedItem()){
							comboBox_2.addItem(t.getNumber());
						}
					}
					comboBox_2.setSelectedIndex(-1);
				}
			}
		});
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GameController gc = new GameController();
				Game g = new Game();
				g.setGameTypeName((String)comboBox.getSelectedItem());
				g.setRoom((int)comboBox_1.getSelectedItem());
				g.setTable((int)comboBox_2.getSelectedItem());
				g.setDealerName((String)comboBox_3.getSelectedItem());				
				gc.addGame(g);			
				dispose();
			}		
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(16)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblRoom)
								.addComponent(lblName)
								.addComponent(lblTable)
								.addComponent(lblDealer))
							.addGap(25)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(comboBox_1, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(comboBox_2, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(comboBox_3, 0, 118, Short.MAX_VALUE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(60)
							.addComponent(btnAdd, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(20, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblName)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRoom)
						.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTable)
						.addComponent(comboBox_2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDealer)
						.addComponent(comboBox_3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnAdd)
					.addContainerGap(17, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
