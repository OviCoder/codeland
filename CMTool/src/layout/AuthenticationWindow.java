package layout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import application.UserContext;
import controllers.AuthenticationController;
import models.Account;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;

@SuppressWarnings("serial")
public class AuthenticationWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private int maxAttempts = 3;
	private AuthenticationController ac;
	private JPasswordField passwordField;

	public AuthenticationWindow() {
		setBounds(100, 100, 236, 160);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		ac = new AuthenticationController();

		JLabel lblFeedbackLabel = new JLabel();
		lblFeedbackLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblFeedbackLabel.setForeground(Color.RED);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setForeground(Color.ORANGE);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setForeground(Color.ORANGE);

		textField = new JTextField();
		textField.setColumns(10);
		
		passwordField = new JPasswordField();

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Account a = ac.requestAccount(textField.getText());
				if (a == null) {
					lblFeedbackLabel.setText("Account does not exist!");
				} else {
					char[] pass = passwordField.getPassword();
					String password = new String(pass);
					if (a.getFailures() < maxAttempts && a.getPassword().equals(password)) {
						a.setFailures(0);
						ac.updateAccount(a);
						UserContext.currentUser = a.getUsername();
						MainWindow window = new MainWindow();
						window.frmCmtool.setVisible(true);
						dispose();
					} else {
						if (a.getFailures() >= maxAttempts) {
							lblFeedbackLabel.setText("Account locked");
							lblFeedbackLabel.repaint();
						} else {
							lblFeedbackLabel.setText("Wrong password. Attempts left: " + (maxAttempts - a.getFailures() - 1));
							lblFeedbackLabel.repaint();
							int x = a.getFailures();
							a.setFailures(x + 1);
							ac.updateAccount(a);
							if(maxAttempts == a.getFailures()){
								lblFeedbackLabel.setText("Account locked");
							}
						}
					}
				}
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblUsername)
						.addComponent(lblPassword))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(passwordField, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
						.addComponent(textField))
					.addGap(16))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblFeedbackLabel, GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
					.addGap(119))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(67)
					.addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(73, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(lblFeedbackLabel)
					.addGap(12)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUsername)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
					.addComponent(btnLogin)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
