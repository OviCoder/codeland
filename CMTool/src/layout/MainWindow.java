package layout;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.SwingConstants;

import application.UserContext;
import controllers.ComplexController;
import controllers.DealerController;
import controllers.GameController;
import controllers.GameTypeController;
import controllers.RoomController;
import controllers.TableController;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import enums.Entity;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;

public class MainWindow {

	public JFrame frmCmtool;
	private JScrollPane scrollPane;
	private DealerController dealerController;
	private GameTypeController gameTypeController;
	private GameController gameController;
	private RoomController roomController;
	private TableController tableController;
	private ComplexController complexController;

	public MainWindow() {
		initialize();
	}

	private void initialize() {
		frmCmtool = new JFrame();
		frmCmtool.setTitle("CMTool");
		frmCmtool.setBounds(100, 100, 850, 400);
		frmCmtool.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCmtool.setResizable(false);
		frmCmtool.setLocationRelativeTo(null);
		scrollPane = new JScrollPane();
		dealerController = new DealerController();
		gameTypeController = new GameTypeController();
		gameController = new GameController();
		roomController = new RoomController();
		tableController = new TableController();
		complexController = new ComplexController();

		JComboBox<Entity> entities = new JComboBox<Entity>();
		entities.setForeground(Color.WHITE);
		entities.addItem(Entity.Dealer);
		entities.addItem(Entity.Game);
		entities.addItem(Entity.Room);
		entities.addItem(Entity.Table);
		entities.addItem(Entity.GameType);
		entities.setSelectedIndex(-1);

		JComboBox<String> operations = new JComboBox<String>();
		operations.setForeground(Color.WHITE);
		operations.addItem("Get dealer balance");
		operations.addItem("Get group balance");
		operations.addItem("Get room balance");
		operations.addItem("Get table balance");
		operations.setSelectedIndex(-1);

		operations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == operations) {
					JComboBox<?> cb = (JComboBox<?>) e.getSource();
					String operation = (String) cb.getSelectedItem();
					UserContext.selectedEntity = null;
					entities.setSelectedIndex(-1);

					if (operation != null) {
						switch (operation) {
						case "Get dealer balance":
							UserContext.chosenOperation = "Get dealer balance";
							break;
						case "Get group balance":
							UserContext.chosenOperation = "Get group balance";
							break;
						case "Get room balance":
							UserContext.chosenOperation = "Get room balance";
							break;
						case "Get table balance":
							UserContext.chosenOperation = "Get table balance";
							break;
						}

						JTable table = complexController.dataBind();
						if (table != null) {
							scrollPane.setViewportView(table);
							scrollPane.repaint();
						}
					}
				}
			}
		});

		entities.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == entities) {
					JComboBox<?> cb = (JComboBox<?>) e.getSource();
					Entity entity = (Entity) cb.getSelectedItem();
					JTable table = null;
					operations.setSelectedIndex(-1);
					UserContext.chosenOperation = null;

					if (entity != null) {
						switch (entity) {
						case Dealer:
							UserContext.selectedEntity = Entity.Dealer;
							table = dealerController.dataBind();
							break;
						case Game:
							UserContext.selectedEntity = Entity.Game;
							table = gameController.dataBind();
							break;
						case Room:
							UserContext.selectedEntity = Entity.Room;
							table = roomController.dataBind();
							break;
						case Table:
							UserContext.selectedEntity = Entity.Table;
							table = tableController.dataBind();
							break;
						case GameType:
							UserContext.selectedEntity = Entity.GameType;
							table = gameTypeController.dataBind();
							break;
						}

						if (table != null) {
							scrollPane.setViewportView(table);
							scrollPane.repaint();
						}
					}
				}
			}
		});

		JLabel lblCasinoManagementTool = new JLabel("Casino Management Tool ");
		lblCasinoManagementTool
				.setIcon(new ImageIcon("C:\\Users\\ovidiu\\Documents\\CodeLand-BB-Repo\\CMTool\\data\\rsz_1dice1.png"));
		lblCasinoManagementTool.setHorizontalAlignment(SwingConstants.TRAILING);
		lblCasinoManagementTool.setFont(new Font("Colonna MT", Font.BOLD | Font.ITALIC, 24));
		lblCasinoManagementTool.setForeground(Color.ORANGE);

		JPanel panel = new JPanel();

		JPanel panel_1 = new JPanel();
		GroupLayout groupLayout = new GroupLayout(frmCmtool.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, 716, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup().addComponent(lblCasinoManagementTool)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)))
				.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(lblCasinoManagementTool)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE).addContainerGap()));

		JLabel lblEntity = new JLabel("ENTITY");
		lblEntity.setForeground(Color.WHITE);

		JLabel lblOperation = new JLabel("OPERATION");
		lblOperation.setForeground(Color.WHITE);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup().addGap(64).addComponent(lblEntity).addGap(114)
						.addComponent(lblOperation).addGap(53))
				.addGroup(gl_panel_1.createSequentialGroup().addContainerGap()
						.addComponent(entities, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE).addGap(18)
						.addComponent(operations, 0, 147, Short.MAX_VALUE).addContainerGap()));
		gl_panel_1
				.setVerticalGroup(
						gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(Alignment.TRAILING,
										gl_panel_1.createSequentialGroup().addContainerGap(38, Short.MAX_VALUE)
												.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
														.addComponent(lblEntity).addComponent(lblOperation))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
										.addComponent(entities, GroupLayout.PREFERRED_SIZE, 20,
												GroupLayout.PREFERRED_SIZE)
								.addComponent(operations, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
						.addContainerGap()));
		panel_1.setLayout(gl_panel_1);

		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFrame aef = null;
				switch (UserContext.selectedEntity) {
				case Dealer:
					aef = new AddDealerWindow();
					break;
				case Game:
					aef = new AddGameWindow();
					break;
				case Room:
					aef = new AddRoomWindow();
					break;
				case Table:
					aef = new AddTableWindow();
					break;
				case GameType:
					aef = new AddGameTypeWindow();
					break;
				}

				if (aef != null) {
					aef.setLocationRelativeTo(frmCmtool);
					aef.setVisible(true);
					frmCmtool.setEnabled(false);
					aef.addWindowListener(new WindowListener() {
						@Override
						public void windowActivated(WindowEvent e) {
						}

						public void windowClosed(WindowEvent e) {
							frmCmtool.setEnabled(true);
							JTable table = null;
							switch (UserContext.selectedEntity) {
							case Dealer:
								table = dealerController.dataBind();
								break;
							case Game:
								table = gameController.dataBind();
								break;
							case Room:
								table = roomController.dataBind();
								break;
							case Table:
								table = tableController.dataBind();
								break;
							case GameType:
								table = gameTypeController.dataBind();
								break;
							}
							if (table != null) {
								scrollPane.setViewportView(table);
								scrollPane.repaint();
							}
						}

						public void windowClosing(WindowEvent e) {
							frmCmtool.setEnabled(true);
							JTable table = null;
							switch (UserContext.selectedEntity) {
							case Dealer:
								table = dealerController.dataBind();
								break;
							case Game:
								table = gameController.dataBind();
								break;
							case Room:
								table = roomController.dataBind();
								break;
							case Table:
								table = tableController.dataBind();
								break;
							case GameType:
								table = gameTypeController.dataBind();
								break;
							}
							if (table != null) {
								scrollPane.setViewportView(table);
								scrollPane.repaint();
							}
						}

						public void windowDeactivated(WindowEvent e) {
						}

						public void windowDeiconified(WindowEvent e) {
						}

						public void windowIconified(WindowEvent e) {
						}

						public void windowOpened(WindowEvent e) {
						}
					});
				}
			}
		});
		btnNew.setForeground(Color.WHITE);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			@SuppressWarnings("incomplete-switch")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JTable table = null;
				switch (UserContext.selectedEntity) {
				case Dealer:
					dealerController.deleteDealer();
					table = dealerController.dataBind();
					break;
				case Room:
					roomController.deleteRoom();
					table = roomController.dataBind();
					break;
				case Table:
					tableController.deleteTable();
					table = tableController.dataBind();
					break;
				case GameType:
					gameTypeController.deleteGameType();
					table = gameTypeController.dataBind();
					break;
				}
				if (table != null) {
					scrollPane.setViewportView(table);
					scrollPane.repaint();
				}
			}
		});
		btnDelete.setForeground(Color.WHITE);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				switch (UserContext.selectedEntity) {
				case Dealer:
					if (dealerController.updateDealers()) {
						JOptionPane.showMessageDialog(frmCmtool, "Data saved.");
						scrollPane.setViewportView(dealerController.dataBind());
						scrollPane.repaint();
					}
					break;
				case Game:
					if (gameController.updateGames()) {
						JOptionPane.showMessageDialog(frmCmtool, "Data saved.");
						scrollPane.setViewportView(gameController.dataBind());
						scrollPane.repaint();
					}
					break;
				case Room:
					if (roomController.updateRooms()) {
						JOptionPane.showMessageDialog(frmCmtool, "Data saved.");
						scrollPane.setViewportView(roomController.dataBind());
						scrollPane.repaint();
					}
					break;
				case Table:
					if (tableController.updateTables()) {
						JOptionPane.showMessageDialog(frmCmtool, "Data saved.");
						scrollPane.setViewportView(tableController.dataBind());
						scrollPane.repaint();
					}
					break;
				case GameType:
					if (gameTypeController.updateGameTypes()) {
						JOptionPane.showMessageDialog(frmCmtool, "Data saved.");
						scrollPane.setViewportView(gameTypeController.dataBind());
						scrollPane.repaint();
					}
					break;
				}
			}
		});
		btnSave.setForeground(Color.WHITE);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 692, Short.MAX_VALUE)
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(btnNew, GroupLayout.PREFERRED_SIZE, 80,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 80,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnSave,
												GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap()));
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
										.addComponent(btnNew, GroupLayout.PREFERRED_SIZE, 16,
												GroupLayout.PREFERRED_SIZE)
								.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnSave, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE).addContainerGap()));
		panel.setLayout(gl_panel);
		frmCmtool.getContentPane().setLayout(groupLayout);
	}
}
