package application;

import enums.Entity;

public final class UserContext {

	public static String currentUser;
	public static Entity selectedEntity;
	public static String chosenOperation;
	
	private UserContext(){
		
		currentUser = null;
		selectedEntity = null;
		chosenOperation = null;
	}
}
