package controllers;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dal.GameTypeDAL;
import models.GameType;
import models.GameTypeTableModel;

public class GameTypeController extends AbstractController {
	private GameTypeDAL gtd;
	private String selectedEntityName;
	private GameTypeTableModel gttm;

	public GameTypeController() {

		gtd = new GameTypeDAL();
		selectedEntityName = null;
	}

	public JTable dataBind() {

		ArrayList<GameType> gameTypes = new ArrayList<GameType>();
		gameTypes = gtd.getAllGameTypes();

		gttm = new GameTypeTableModel(gameTypes);
		JTable table = new JTable(gttm);

		ListSelectionModel tableLSM = table.getSelectionModel();
		tableLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableLSM.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!tableLSM.isSelectionEmpty()) {
					int selectedGameType = table.convertRowIndexToModel(tableLSM.getMinSelectionIndex());
					selectedEntityName = ((GameTypeTableModel) gttm).getGameTypeAt(selectedGameType).getName();
				}
			}
		});
		return table;
	}
	
	public void addGameType(final GameType gameType){	
		gtd.addGameType(gameType);
	}
	
	public void deleteGameType(){
		if(this.selectedEntityName != null){
				gtd.deleteGameType(selectedEntityName);
		}
	}
	
	public boolean updateGameTypes(){
		return gtd.updateGameTypes(gttm.getModifiedGameTypes());
	}
}
