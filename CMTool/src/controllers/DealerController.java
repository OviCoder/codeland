package controllers;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dal.DealerDAL;
import models.Dealer;
import models.DealerTableModel;

public class DealerController extends AbstractController{

	private DealerDAL dd;
	private int selectedEntityID;
	private DealerTableModel dtm;
	
	public DealerController(){
		
		dd = new DealerDAL();
		selectedEntityID = -1;
	}
	
	public JTable dataBind(){
		
		ArrayList<Dealer> dealers = new ArrayList<Dealer>();
		dealers = dd.getAllDealers();
		
		dtm = new DealerTableModel(dealers);
		JTable table = new JTable(dtm);
		
		ListSelectionModel tableLSM = table.getSelectionModel();
		tableLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableLSM.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				if (!tableLSM.isSelectionEmpty())
				{
					int selectedDealer = table.convertRowIndexToModel(tableLSM.getMinSelectionIndex());
					selectedEntityID = ((DealerTableModel) dtm).getDealerAt(selectedDealer).getId();
				}
			}
		});
		return table;
	}
	
	public void addDealer(final Dealer dealer){	
		dd.addDealer(dealer);
	}
	
	public void deleteDealer(){
		if(this.selectedEntityID != -1){
				dd.deleteDealer(selectedEntityID);
		}
	}
	
	public boolean updateDealers(){
		return dd.updateDealers(dtm.getModifiedDealers());
	}
}
