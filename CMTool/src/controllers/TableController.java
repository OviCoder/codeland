package controllers;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dal.TableDAL;
import models.Table;
import models.TableTableModel;

public class TableController extends AbstractController {
	private TableDAL td;
	private int selectedEntityID;
	private TableTableModel ttm;

	public TableController() {
		td = new TableDAL();
		selectedEntityID = -1;
	}

	public JTable dataBind() {

		ArrayList<Table> tables = new ArrayList<Table>();
		tables = td.getAllTables();

		ttm = new TableTableModel(tables);
		JTable table = new JTable(ttm);

		ListSelectionModel tableLSM = table.getSelectionModel();
		tableLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableLSM.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!tableLSM.isSelectionEmpty()) {
					int selectedTable = table.convertRowIndexToModel(tableLSM.getMinSelectionIndex());
					selectedEntityID = ttm.getTableAt(selectedTable).getId();
				}
			}
		});
		return table;
	}
	
	public void addTable(final Table table){	
		td.addTable(table);
	}
	
	public void deleteTable(){
		if(this.selectedEntityID != -1){
				td.deleteTable(selectedEntityID);
		}
	}
	
	public boolean updateTables(){
		return td.updateTables(ttm.getModifiedTables());
	}
}
