package controllers;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dal.RoomDAL;
import models.Room;
import models.RoomTableModel;

public class RoomController extends AbstractController {
	private RoomDAL rd;
	private int selectedEntityNumber;
	private RoomTableModel rtm;

	public RoomController() {
		rd = new RoomDAL();
		selectedEntityNumber = -1;
	}

	public JTable dataBind() {

		ArrayList<Room> rooms = new ArrayList<Room>();
		rooms = rd.getAllRooms();

		rtm = new RoomTableModel(rooms);
		JTable table = new JTable(rtm);

		ListSelectionModel tableLSM = table.getSelectionModel();
		tableLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableLSM.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!tableLSM.isSelectionEmpty()) {
					int selectedRoom = table.convertRowIndexToModel(tableLSM.getMinSelectionIndex());
					selectedEntityNumber = rtm.getRoomAt(selectedRoom).getNumber();
				}
			}
		});
		return table;
	}
	
	public void addRoom(final Room room){	
		rd.addRoom(room);
	}
	
	public void deleteRoom(){
		if(this.selectedEntityNumber != -1){
				rd.deleteRoom(selectedEntityNumber);
		}
	}
	
	public boolean updateRooms(){
		return rd.updateRooms(rtm.getModifiedRooms());
	}
}
