package controllers;

import dal.AuthenticationDAL;
import models.Account;

public class AuthenticationController {
	private AuthenticationDAL ad;
	private Account a;
	
	public AuthenticationController(){
		ad = new AuthenticationDAL();
		a = null;
	}
	
	public Account requestAccount(String user){	
		a = ad.requestAccount(user);
		return a;
	}
	
	public void updateAccount(Account a){
		ad.updateAccount(a);
	}
}
