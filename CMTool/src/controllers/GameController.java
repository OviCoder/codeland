package controllers;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import dal.GameDAL;
import models.Game;
import models.GameTableModel;

public class GameController extends AbstractController {

	private GameDAL gd;
	@SuppressWarnings("unused")
	private int selectedEntityID;
	private GameTableModel gtm;

	public GameController() {

		gd = new GameDAL();
		selectedEntityID = -1;
	}

	public JTable dataBind() {

		ArrayList<Game> games = new ArrayList<Game>();
		games = gd.getAllGames();

		gtm = new GameTableModel(games);
		JTable table = new JTable(gtm);

		ListSelectionModel tableLSM = table.getSelectionModel();
		tableLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableLSM.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!tableLSM.isSelectionEmpty()) {
					int selectedGame = table.convertRowIndexToModel(tableLSM.getMinSelectionIndex());
					selectedEntityID = ((GameTableModel) gtm).getGameAt(selectedGame).getId();
				}
			}
		});
		return table;
	}
	
	public void addGame(final Game game){	
		gd.addGame(game);
	}
	
	
	public boolean updateGames(){
		return gd.updateGames(gtm.getModifiedGames());
	}
}
