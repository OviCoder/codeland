package controllers;

import java.util.ArrayList;

import javax.swing.JTable;

import application.UserContext;
import dal.ComplexDAL;
import models.complex.DealerBalance;
import models.complex.DealerBalanceTableModel;
import models.complex.GroupBalance;
import models.complex.GroupBalanceTableModel;
import models.complex.RoomBalance;
import models.complex.RoomBalanceTableModel;
import models.complex.TableBalance;
import models.complex.TableBalanceTableModel;

public class ComplexController {
	private DealerBalanceTableModel dbtm;
	private RoomBalanceTableModel rbtm;
	private TableBalanceTableModel tbtm;
	private GroupBalanceTableModel gbtm;
	private ComplexDAL cd;
	
	public ComplexController(){
		cd = new ComplexDAL();
	}
	
	public JTable dataBind(){
		
		JTable table = null;
		
		switch (UserContext.chosenOperation) {
		case "Get dealer balance":
			ArrayList<DealerBalance> dbs = new ArrayList<DealerBalance>();
			dbs = cd.getDealersBalance();
			dbtm = new DealerBalanceTableModel(dbs);
			table = new JTable(dbtm);				
			break;
		case "Get group balance":
			ArrayList<GroupBalance> gbs = new ArrayList<GroupBalance>();
			gbs = cd.getGroupsBalance();
			gbtm = new GroupBalanceTableModel(gbs);
			table = new JTable(gbtm);
			break;
		case "Get room balance":
			ArrayList<RoomBalance> rbs = new ArrayList<RoomBalance>();
			rbs = cd.getRoomsBalance();
			rbtm = new RoomBalanceTableModel(rbs);
			table = new JTable(rbtm);
			break;
		case "Get table balance":
			ArrayList<TableBalance> tbs = new ArrayList<TableBalance>();
			tbs = cd.getTablesBalance();
			tbtm = new TableBalanceTableModel(tbs);
			table = new JTable(tbtm);
			break;
		}
		
		return table;
	}
}
