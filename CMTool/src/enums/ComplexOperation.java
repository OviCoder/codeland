package enums;

public enum ComplexOperation {
	getDealerBalance,
	getGroupBalance,
	getRoomBalance,
	getTableBalance
}
