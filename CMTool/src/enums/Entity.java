package enums;

public enum Entity {
	Dealer,
	Game,
	Room,
	Table,
	GameType
}
