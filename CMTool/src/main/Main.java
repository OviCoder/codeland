package main;

import java.awt.EventQueue;

import javax.swing.UIManager;

import layout.AuthenticationWindow;

public final class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");							
					AuthenticationWindow w = new AuthenticationWindow();
					w.setLocationRelativeTo(null);
					w.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
